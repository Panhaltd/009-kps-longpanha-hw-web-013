import React, { Component } from 'react'
import Axios from 'axios';
import { Link } from 'react-router-dom';
import { Button, Table } from 'react-bootstrap';
import './HomeStyle.css'
import Pagination from 'react-js-pagination'

export default class HomePage extends Component {

    constructor(){
        super();
        this.state={
            data:[],
            activePage:1,
            perPage:5,
        }
    }

    componentWillMount(){
        Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=35")
        .then(res=>{
          console.log(res.data.DATA);
            this.setState({
                data:res.data.DATA,
            })
        }).catch(err=>{
            console.log(err);
        })
    }

    componentWillUpdate(){
        Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=35")
        .then(res=>{
          console.log(res.data.DATA);
            this.setState({
                data:res.data.DATA,
            })
        }).catch(err=>{
            console.log(err);
        })
    }

    convertDate=(dateStr)=>{
        var dateString = dateStr;
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = year + "-" + month + "-" + day;
        return date;
    }

    deleteArticle= (idItem) =>{
        console.log(idItem);
        Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${idItem}`)
        .then(res=>{
            console.log(res.data.MESSAGE);
            this.componentWillMount()
        }).catch(err=>{
            console.log(err);
            
        })
    }
    
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});
    }

    render() {
        const indexOfLast =this.state.activePage * this.state.perPage;
        const indexOfFirst = indexOfLast - this.state.perPage;
        const current = this.state.data.slice(indexOfFirst, indexOfLast);
        var items = current.map((item) => (
            <tr key={item.ID}>
            <td style={{ width: "10%" }}>{item.ID}</td>
            <td style={{ width: "15%" }}>{item.TITLE}</td>
            <td style={{ width: "30%" }}>{item.DESCRIPTION}</td>
            <td style={{ width: "10%" }}>{this.convertDate(item.CREATED_DATE)}</td> 
            <td style={{ width: "10%" }}>
                <img style={{ width: 100 }} src={item.IMAGE} alt="article" />
            </td>
            <td style={{ width: "25%" }}>  
                <Link to={`/View/${item.ID}`}>
                <Button style={{ margin: 2 }}>View</Button>
                </Link>
                <Link to={`/Update/${item.ID}`}>
                <Button style={{ margin: 2 }} variant="warning">Edit</Button>
                </Link>
                <Button onClick={()=>this.deleteArticle(item.ID)} style={{ margin: 2}} variant="danger">Delete</Button>
            </td>
            </tr>
        ));
        return (
            <div className="container">
                <h2 style={{ margin: 15 }}>Article Manangement</h2>
                <Link to="/Add"><Button variant="dark">Add New Article</Button></Link>
                <Table style={{marginTop:14}} striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th style={{ width: "10%" }}>#</th>
                        <th style={{ width: "15%" }}>Title</th>
                        <th style={{ width: "30%" }}>Description</th>
                        <th style={{ width: "10%" }}>Created Date</th>
                        <th style={{ width: "10%" }}>Image</th>
                        <th style={{ width: "25%" }}>Action</th>
                    </tr>
                    </thead>
                    <tbody>{items}</tbody>
                </Table>
                <div style={{marginLeft:"35%"}}>
                <Pagination
                    prevPageText='prev'
                    nextPageText='next'
                    firstPageText='first'
                    lastPageText='last'
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.perPage}
                    totalItemsCount={this.state.data.length}
                    itemClass="page-item"
                    linkClass="page-link"
                    onChange={this.handlePageChange.bind(this)}
                 />
                </div>
            </div>
        )
    }
}
