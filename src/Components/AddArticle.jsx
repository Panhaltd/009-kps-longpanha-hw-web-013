import React, { Component } from 'react'
import './AddStyle.css'
import ImgHolder from '../placeholder-img.jpg'
import { Link, Redirect } from 'react-router-dom';
import Axios from 'axios';
import { Button } from 'react-bootstrap';

export default class AddArticle extends Component {

    constructor(){
        super();
        this.state={
            isUpdate:false,
            title:"",
            titleWarn:"",
            descriptionWarn:"",
            description:"",
            image:ImgHolder,
            selectedFile:"",
        }
    }

    handleTitle=(event)=>{
        const value = event.target.value;
        this.setState({
            title:value
        })
    }

    handleDescription=(event)=>{
        const value = event.target.value;
        this.setState({
            description:value
        })
    }

    fileSelectedHandler=(event)=>{
        this.refs.fileUploader.click();
    }

    onImageChange = (event) => {
        console.log(event);
        if (event.target.files && event.target.files[0]) {
          this.setState({
            image: URL.createObjectURL(event.target.files[0]),
          });
        }
    };

    handleImageChange=(event)=>{
        console.log("Hi");
        var preview = document.getElementById("blah");
        var file = event.target.files[0];
        var reader = new FileReader();
        if (file) {
          reader.readAsDataURL(file);
        } else {
          preview.src = ImgHolder;
        }
        reader.onloadend = () => {
          preview.src = reader.result;
          
          this.setState({ 
              image:preview.src,
              selectedFile: reader.result
            });
        };
        this.setState({ selectedFile: file });
    }


    validate=()=>{
        if(this.state.title==""&&this.state.description==""){
          this.setState({
            titleWarn:" : Title cannot be null",
            descriptionWarn:" : Description cannot be null",
          })
          return false
        }else if(this.state.title==""&&this.state.description!=""){
          this.setState({
            titleWarn:" : Title cannot be null",
            descriptionWarn:"",
          })
          return false
        }else if(this.state.title!=""&&this.state.description==""){
          this.setState({
            titleWarn:"",
            descriptionWarn:" : Description cannot be null",
          })
          return false
        }else{
          this.setState({
            titleWarn:"",
            descriptionWarn:""
          })
          return true
        }
      }

    upload=()=>{
        const isValid=this.validate();
        if(isValid==true){
            let article = {
                TITLE: this.state.title,
                DESCRIPTION: this.state.description,
                IMAGE: this.state.image,
              };
              Axios.post("http://110.74.194.124:15011/v1/api/articles", article)
              .then(res => {
                  console.log(res.data.MESSAGE);
                  this.setState({
                      isUpdate:true
                  })
                  alert("Add Success")
                  this.props.history.push("/");
                }
              ).catch(err=>{
                console.log(err);
              });
        }else{
        }
    }

    render() {
        console.log(this.state.titleWarn);
        console.log(this.state.descriptionWarn);
        return (
            <div className="container" style={{textAlign:"left",padding:20}}>
                <div className="row">
                    <div className="col-md-8">
                        <h2 style={{color:"darkBlue"}}><u>Add Article</u></h2>
                        <label>Title</label><span style={{color:"red"}}>{this.state.titleWarn}</span><br/>
                        <input type="text" onChange={this.handleTitle} placeholder="Enter Title" /><br/>
                        <label>Description</label><span style={{color:"red"}}>{this.state.descriptionWarn}</span><br/>
                        <textarea onChange={this.handleDescription} placeholder="Enter Description"  />
                        <Link><Button style={{marginTop:6,backgroundColor:"darkBlue"}} onClick={this.upload}>Upload</Button></Link>
                    </div>
                    <div className="col-md-4">
                        <div className="img-con" onClick={this.fileSelectedHandler} >
                            <img id="blah" src={this.state.image} style={{width:"100%"}}/>
                            <input type="file" style={{display:"none"}} onChange={this.handleImageChange} ref="fileUploader" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
