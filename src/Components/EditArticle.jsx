import React, { Component } from 'react'
import ImgHolder from '../placeholder-img.jpg'
import Axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import './AddStyle.css';

export default class EditArticle extends Component {

    constructor() {
        super();
        this.state={
            data:[],
            title:"",
            description:"",
            titleWarn:"",
            descriptionWarn:"",
            image:"",
            isUpdate:false,
        }
    }

    componentDidMount(){
        Axios.get(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`)
        .then(res=>{
          console.log(res.data.DATA);
            this.setState({
                data:res.data.DATA,
            })
            this.setState({
                title:this.state.data.TITLE,
                description:this.state.data.DESCRIPTION,
                image:this.state.data.IMAGE
            })
            
        }).catch(err=>{
            console.log(err);
        })
    }

    validate=()=>{
        if(this.state.title==""&&this.state.description==""){
          this.setState({
            titleWarn:" : Title cannot be null",
            descriptionWarn:" : Description cannot be null",
          })
          return false
        }else if(this.state.title==""&&this.state.description!=""){
          this.setState({
            titleWarn:" : Title cannot be null",
            descriptionWarn:"",
          })
          return false
        }else if(this.state.title!=""&&this.state.description==""){
          this.setState({
            titleWarn:"",
            descriptionWarn:" : Description cannot be null",
          })
          return false
        }else{
          this.setState({
            titleWarn:"",
            descriptionWarn:""
          })
          return true
        }
      }

    update=()=>{
        if(this.validate==true){
            let article = {
                TITLE: this.state.title,
                DESCRIPTION: this.state.description,
                IMAGE: this.state.image,
            };
            Axios.put(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`,article)
            .then(res=>{
                    console.log(this.props.match.params.id);
                    console.log(res.data.MESSAGE);
                    alert("Update Success");
                    this.props.history.push("/");
            }).catch(err=>
                console.log(err)
            )
        }
    }

    handleTitle=(event)=>{
        const value = event.target.value;
        this.setState({
            title:value
        })
    }

    handleDescription=(event)=>{
        const value = event.target.value;
        this.setState({
            description:value
        })
    }

    fileSelectedHandler=(event)=>{
        this.refs.fileUploader.click();
    }

    handleImageChange=(event)=>{
        console.log("Hi");
        var preview = document.getElementById("blah");
        var file = event.target.files[0];
        var reader = new FileReader();
        if (file) {
          reader.readAsDataURL(file);
        } else {
          preview.src = ImgHolder;
        }
        reader.onloadend = () => {
          preview.src = reader.result;
          this.setState({ 
              image:preview.src,
              selectedFile: reader.result
            });
        };
        this.setState({ selectedFile: file });
    }
    render() {
        return (
            <div className="container" style={{textAlign:"left",padding:20}}>
                <div className="row">
                    <div className="col-md-8">
                        <h2 style={{color:"darkBlue"}}>Edit Article</h2>
                        <label >Title</label><span style={{color:"red"}}>{this.state.titleWarn}</span><br/>
                        <input type="text" onChange={this.handleTitle} placeholder="Enter Title" value={this.state.title} /><br/>
                        <label>Description</label><span style={{color:"red"}}>{this.state.descriptionWarn}</span><br/>
                        <textarea onChange={this.handleDescription} placeholder="Enter Description" value={this.state.description}  />
                        <Button onClick={this.update}>Update</Button>
                    </div>
                    <div className="col-md-4">
                        <div className={this.state.image!=""?"img-con":""} onClick={this.fileSelectedHandler} >
                            <img id="blah" src={this.state.image} style={{width:"100%"}}/>
                            <input type="file" style={{display:"none"}} onChange={this.handleImageChange} ref="fileUploader" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
