import React, { Component } from 'react'
import Axios from 'axios'
import './ViewStyle.css'

export default class ViewPre extends Component {
    constructor(props){
        super(props);
        this.state={
            data:[],
            isGetData:false,
        }
    }

    componentWillMount(){
        Axios.get(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`)
        .then(res=>{
          console.log(res.data.DATA);
            this.setState({
                data:res.data.DATA,
                isGetData:true,
            })
        }).catch(err=>{
            console.log(err);
        })
    }

    convertDate=(dateStr)=>{
        if(this.state.isGetData){
            var dateString = dateStr;
            var year = dateString.substring(0, 4);
            var month = dateString.substring(4, 6);
            var day = dateString.substring(6, 8);
            var date = "Posted on "+ day + "-" + month + "-" +year;
            return date;
        }
    }

    render() {
        return (
            <div>
                <div className="container" style={{marginTop:30}}>
                <div className="row" style={{padding:20,backgroundColor:"#EEE",borderRadius:10,textAlign:"left"}}>
                    <div className="col-md-4" style={{padding:0}}>
                        <img style={{borderRadius:10}} className="w-100" src={this.state.data.IMAGE} alt="Movie"/>
                    </div>
                    <div className="col-md-8" style={{color:"#000",paddingLeft:20}}>
                        <div className="title-con">
                            <h3>{this.state.data.TITLE}</h3>
                            <h5>ID: {this.state.data.ID}</h5>
                            <h6 style={{color:"#605a56"}}>{this.convertDate(this.state.data.CREATED_DATE)}</h6>
                            <h6></h6>
                        </div>
                        <p style={{fontSize:20}}>{this.state.data.DESCRIPTION}</p>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}
