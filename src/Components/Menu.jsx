import React, { Component } from 'react'
import {Nav,Navbar,Form,FormControl,Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default class Menu extends Component {

    constructor(){
        super();
        this.state={
            search:""
        }
    }

    handleTitle=(event)=>{
        const value = event.target.value;
        this.setState({
            search:value
        })
    }

    render(){
        return (
            <div>
                <Navbar bg="light" variant="light">
                    <div className="container">
                    <Navbar.Brand as={Link} to="/">AMS</Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/Home">Home</Nav.Link>
                    </Nav>
                    <Form inline>
                    <FormControl onChange={this.handleTitle} type="text" placeholder="Search" className="mr-sm-2" />
                    <Link to={`/Search/${this.state.search}`}><Button variant="outline-info">Search</Button></Link>
                    </Form>
                    </div>
                </Navbar>
            </div>
        )
    }
}
