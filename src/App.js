import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import Menu from './Components/Menu';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react'
import AddArticle from './Components/AddArticle';
import ViewPre from './Components/ViewPre';
import HomePage from './Components/HomePage';
import './App.css'
import EditArticle from './Components/EditArticle';
import Search from './Components/Search';
export default class App extends Component {

  render() {
    return (
      <div className="App">
        <Router>
          <Menu/>
          <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/View/:id" component={ViewPre} />
              <Route path="/Add" component={AddArticle}/>
              <Route path="/Update/:id" component={EditArticle} />
              <Route path="/Search/:title" component={Search} />
          </Switch>
        </Router>
      </div>
    )
  }
}

